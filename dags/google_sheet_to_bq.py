import logging
import os
import re
import sys
import json

# TODO: this is an easy way to fix the import path on this file, should be removed once we figure out a way around.
sys.path.insert(0, f'{os.path.dirname(__file__)}/')

from airflow import DAG
from airflow.contrib.operators.bigquery_table_delete_operator import BigQueryTableDeleteOperator
from common import (GCP_CONN_ID, get_dag_default_args, load_config, get_dags_folder,
                    read_local_file)

from operators.bigquery_google_sheet_operator import BigQueryCreateGoogleSheetTableOperator

def generate_dag():
    """Returns a DAG to satisfy the request."""
    dag = DAG(
        'google_sheet_to_bigquery',
        default_args=get_dag_default_args(),
        schedule_interval='0 23 * * *',
        catchup=False,
        max_active_runs=1)

    conf = load_config()

    for job_config in conf['google_sheet_to_bq_jobs']:
        delete_task = BigQueryTableDeleteOperator(
            dag=dag,
            task_id=f'delete_bq_if_exists_table_{job_config["name"]}',
            deletion_dataset_table=job_config['dest_project_dataset_table'],
            bigquery_conn_id=GCP_CONN_ID,
            ignore_if_missing=True,
        )

        create_table_task = BigQueryCreateGoogleSheetTableOperator(
            dag=dag,
            task_id=f'create_external_table_{job_config["name"]}',
            skip_leading_rows=job_config["skip_leading_rows"],
            source_format='GOOGLE_SHEETS',
            schema_fields=read_table_schema(job_config["table_schema"]),
            source_uris=job_config["source_uris"],
            max_bad_records=job_config["max_bad_records"],
            ignore_unknown_values=job_config["ignore_unknown_values"],
            bigquery_conn_id=GCP_CONN_ID,
            destination_project_dataset_table=job_config['dest_project_dataset_table']
        )

        create_table_task << delete_task

    return dag

def read_table_schema(filename: str) -> str:
    """Reads a table schema where filename is either an absolute path or a relative path from the DAGs folder"""
    dags_folder = get_dags_folder()
    filename = filename if filename.startswith('/') else os.path.join(dags_folder, filename)
    with open(filename) as filename:
        return json.load(filename)


GOOGLE_SHEET_TO_BQ_DAG = generate_dag()
