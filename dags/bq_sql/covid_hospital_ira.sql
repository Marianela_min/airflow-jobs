-- This script transforms events from the "Encuesta IRA graves" Google form, which is published
-- to PubSub as a JSON with answers to all the questions in the form.
--
-- An example payload can be found at covid_payload_example.json
SELECT DISTINCT
  JSON_EXTRACT_SCALAR(payload, '$.0') AS timestamp,
  REGEXP_EXTRACT(JSON_EXTRACT_SCALAR(payload, '$.1'), r"\(([0-9-]+)\)") AS report_day,
  JSON_EXTRACT_SCALAR(payload, '$.2') AS hospital_code,
  JSON_EXTRACT_SCALAR(payload, '$.3') AS federal_entity,
  JSON_EXTRACT_SCALAR(payload, '$.4') AS hospital_type,
  JSON_EXTRACT_SCALAR(payload, '$.5') AS administrative_entity,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.6') AS int64) AS icu_beds_count,
  JSON_EXTRACT_SCALAR(payload, '$.7') AS icu_beds_added_since_last_report,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.8') AS int64) AS er_beds_count,
  JSON_EXTRACT_SCALAR(payload, '$.9') AS er_beds_added_since_last_report,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.10') AS int64) AS respiratory_units_count,
  JSON_EXTRACT_SCALAR(payload, '$.11') AS respiratory_units_added_since_last_report,
  JSON_EXTRACT_SCALAR(payload, '$.12') AS adhoc_ira_capacity_added,
  JSON_EXTRACT_SCALAR(payload, '$.13') AS adhoc_ira_capacity_respiratory_units_available,
  JSON_EXTRACT_SCALAR(payload, '$.14') AS adhoc_tents_ira_capacity_added,
  JSON_EXTRACT_SCALAR(payload, '$.15') AS masks_available,
  JSON_EXTRACT_SCALAR(payload, '$.16') AS gloves_available,
  JSON_EXTRACT_SCALAR(payload, '$.17') AS soap_available,
  JSON_EXTRACT_SCALAR(payload, '$.18') AS hand_sanitizer_available,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.19') AS int64) AS ira_cases_in_uci_count,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.20') AS int64) AS other_cases_in_uci_count,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.21') AS int64) AS ira_cases_in_er_count,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.22') AS int64) AS other_cases_in_er_count,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.23') AS int64) AS ira_cases_in_respiratory_count,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, '$.24') AS int64) AS ira_death_count,
  JSON_EXTRACT_SCALAR(payload, '$.25') AS power_availability,
  JSON_EXTRACT_SCALAR(payload, '$.26') AS wash_availability
FROM `{{ params.raw_events_table }}`
WHERE event = 'covid_hospital_ira_status'
