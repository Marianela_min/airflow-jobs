  /* ******************************************************************************

Table definitions:

    monitor_ciudad_agua

      This is the result of the current query.


    monitor_ciudad_agua2
      
      This table contains the information I want to show the user, all related to the report from the surveys,
      however information regarding location: municipio, parroquia and entidad are displyed in codes,
      this needs to be mapped and converted into strings containing the actual names of those places.


    parroquias_ref

      this table contains all unique parroquias with their code and string name

          -----------------------------------     
          | parroquia_code | parroquia_name |
          -----------------------------------
          |                |                |


    municipios_ref

      this table contains all unique municipios with their code and string name
          -----------------------------------     
          | municipio_code | municipio_name |
          -----------------------------------
          |                |                |


****************************************************************************** */
WITH
  monitor_ciudad_agua2 AS (

    /*
                            SCHEMA - Monitor Ciudad
                                  Julio 9, 2020

              I used the EsquemaAlias.xls file as a guide to name the variables. They were splitted as the following sections show:

                - Regarding location [loc]
                - Regarding water service [agua]
                - Regarding Electricity service [elec]
                - Regarding residential gas [gas]
                - Regarding the report (metadata) [reporte]

              The Scheme can be found on google sheets in the following url:

                https://docs.google.com/spreadsheets/d/1g9TYcBRbYAZoIHAPQBKrt-pt24kgzK_K12v8km5Bki4/edit?usp=sharing
                
               
              Contributors:
              - Diego Gimenez
              - Marianela Crissman

              */
    ------------------------------------------------------------------------------------------------------
    /*
                Location

    */
    ------------------------------------------------------------------------------------------------------
  SELECT
    DISTINCT 
--     payload,
    -- JSON_EXTRACT_SCALAR(payload,
    --     "$['globalid']") AS global_id,
    -- ID Unico para cada reporte. El Global ID lo hace automaticamente para cada reporte. El usuario no sabe de este ID.
    -- PILAS CON ESTO PARA EL COMMIT
    JSON_EXTRACT_SCALAR(payload,
        "$['entidad' ]") AS loc_entidad,
    JSON_EXTRACT_SCALAR(payload,
        "$['municipio']") AS loc_municipio_id,
    JSON_EXTRACT_SCALAR(payload,
        "$['parroquia']") AS loc_parroquia_id,
    JSON_EXTRACT_SCALAR(payload,
        "$['sector']") AS loc_sector,
    SAFE_CAST (JSON_EXTRACT_SCALAR(payload,
        "$['coordx']") AS FLOAT64) AS loc_coordx_temp,
    SAFE_CAST (JSON_EXTRACT_SCALAR(payload,
        "$['coordy']") AS FLOAT64) AS loc_coordy_temp,
    CONCAT( '(',SAFE_CAST (JSON_EXTRACT_SCALAR(payload,
          "$['coordx']") AS FLOAT64), ',', SAFE_CAST (JSON_EXTRACT_SCALAR(payload,
          "$['coordy']") AS FLOAT64), ')' ) AS loc_coord_temp,
    -- JSON_EXTRACT(payload,   "$['coord' ]") 											                    AS coord,
    -- hidro: Valor Calculado. Parece que esta es la hidrologica regional. Tiene varios en lugar de valores nulos (comparado con campo hidrologica)
    -- JSON_EXTRACT(payload,   "$['hidro']")                                           AS hidro, --
    JSON_EXTRACT_SCALAR(payload,
        "$['hidrologica']") AS agua_hidrologica,
    -- Tiene los mismos valores de hidro pero tiene valores nulos.
    -- JSON_EXTRACT(payload,   "$['fecha_inicio']")                                    AS fecha_inicio, -- Validar este campo
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['semana_reporte']") AS INT64) AS reporte_semana_nro,
    SAFE_CAST (JSON_EXTRACT(payload,
        "$['dia_semana_inicio']") AS FLOAT64) AS _dia_semana_inicio,
    DATETIME( TIMESTAMP_MILLIS( SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['inicio_semana']") as INT64) ), 'America/Caracas'
        ) as reporte_semana_inicio,
    DATETIME( TIMESTAMP_MILLIS( SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['fin_semana']") as INT64) ), 'America/Caracas'
        ) as reporte_semana_fin,
    ------------------------------------------------------------------------------------------------------
    /*
                          Water Service

    */ 
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['suministro_semanal']") AS INT64 )AS agua_suministro_semanal,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['agua_interrupcion']") AS INT64) AS agua_interrupcion_dias,
    JSON_EXTRACT_SCALAR(payload,
        "$['agua_dias']") AS agua_recibida_dias,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['lunes']") AS INT64) AS agua_horas_lunes,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['martes']") AS INT64) AS agua_horas_martes,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['miercoles']") AS INT64) AS agua_horas_miercoles,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['jueves']") AS INT64) AS agua_horas_jueves,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['viernes']") AS INT64) AS agua_horas_viernes,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['sabado']") AS INT64) AS agua_horas_sabado,
    SAFE_CAST(JSON_EXTRACT_SCALAR(payload,
        "$['domingo']") AS INT64) AS agua_horas_domingo,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['horas_entresemana']") AS INT64) AS agua_horas_dialaboral,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['horas_finsemana']") AS INT64) AS agua_horas_finsemana,
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['horas_aguasemana']") AS INT64) AS agua_horas_semana,
    -- Ciclos en los que hidrocapital envia agua en CCS.
    -- Ciclo1: Domingo a Miercoles
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['ciclo1']") AS INT64) AS agua_horas_ciclo1,
    -- Ciclo2: Miercoles a Sabado
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['ciclo2']") AS INT64) AS agua_horas_ciclo2,
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['dias_sinagua']") AS INT64) AS agua_sin_servicio_dias,
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['frecuencia']") AS INT64) AS agua_frecuencia,
    SAFE_CAST ( JSON_EXTRACT_SCALAR(payload,
        "$['otra_frecuencia']") AS INT64) AS agua_otra_frecuencia_semanas,
    JSON_EXTRACT_SCALAR(payload,
        "$['acceso']") AS agua_acceso,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['dias_tanque']") AS INT64) AS agua_almacenada_dias,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['llenado_tanque']") AS INT64) AS agua_tanque_llenado,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['horas_tanque']") AS INT64) AS agua_tanque_recoleccion_horas,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['costo_cisterna']") AS INT64) AS agua_cisterna_costo,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['acarreo_cisterna']") AS INT64) AS agua_bool_cisterna_en_tobos,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['acarreo']") AS INT64) AS agua_bool_acarreo_pago,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['costo_acarreo']") AS INT64) AS agua_acarreo_costo,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['horas_acarreo']") AS INT64) AS agua_acarreo_horas,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['higiene']") AS INT64) AS agua_bool_mantener_higiene,
    JSON_EXTRACT_SCALAR(payload,
        "$['comentario_sinagua']") AS agua_comentario,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['calidad_agua']") AS INT64) AS agua_bool_calidad,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['p_organoleptica']") AS INT64) AS agua_calidad_caract,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['potabilizacion']") AS INT64) AS agua_potabilizacion,
    JSON_EXTRACT_SCALAR(payload,
        "$['potabilizacion_other']") AS agua_potabilizacion_otro,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['pagar_aguabot']") AS INT64) AS agua_costo_botella,
    -----------------------------------------------------------------------------------------------------
    /*
                          Electricity Service

    */
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['interrupcion_elect']") AS INT64) AS elec_bool_interrupcion,
--     SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
--         "$['electricidad_dias']") AS INT64) AS elec_interrupcion_dias,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['lunes_e']") AS INT64) AS elec_horas_lunes,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['martes_e']") AS INT64) AS elec_horas_martes,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['miercoles_e']") AS INT64) AS elec_horas_miercoles,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['jueves_e']") AS INT64) AS elec_horas_jueves,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['viernes_e']") AS INT64) AS elec_horas_viernes,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['sabado_e']") AS INT64) AS elec_horas_sabado,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['domingo_e']") AS INT64) AS elec_horas_domingo,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['apagon_dialaboral']") AS INT64) AS elec_horas_dialaboral,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['apagon_finsemana']") AS INT64) AS elec_horas_finsemana,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['apagon_semana']") AS INT64) AS elec_horas_semana,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['bajones_e']") AS INT64) AS elec_bool_danos_col,
    JSON_EXTRACT_SCALAR(payload,
        "$['comentario_elec']") AS elec_comentario,
    ------------------------------------------------------------------------------------------------------
    /*

                            Residential Gas

    */
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['gas_semana']") AS INT64) AS gas_fuente,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['gas_tiempo']") AS INT64) AS gas_conseguir_bombona_dias,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['gas_tipobombona']") AS INT64) AS gas_tipo_bombona,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['gas_costo']") AS INT64) AS gas_costo_bombona,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['gas_camion']") AS INT64) AS gas_bool_camion,
    JSON_EXTRACT_SCALAR(payload,
        "$['comentario_gas']") AS gas_comentario,
    ------------------------------------------------------------------------------------------------------
    /*

              Metadata 

              Considere metadata todo aquello relacionado a: 
                - Info personal (correo, ID, habitantes de parroquia, codigo monitor) como 
                - IDs o timestamps generados por el sistema (inicio_reporte, fin_reporte, tiempo_llenado,EditDate, GlobalID, CreationDate)
              */

    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['hab']") AS INT64) AS loc_habitantes_parroquia,
    -- Cuantas personas estan siendo afectadas en la parroquia.
    DATETIME( TIMESTAMP_MILLIS( SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['inicio_reporte']") as INT64) ), 'America/Caracas'
        ) AS reporte_hora_inicio,
    DATETIME( TIMESTAMP_MILLIS( SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['fin_reporte']") as INT64) ), 'America/Caracas'
        ) AS reporte_hora_fin,
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['tiempo_llenado']") AS FLOAT64) AS reporte_tiempo_llenado,
    -- Tiempo que usuario lleno encuesta
    SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
        "$['seguimiento']") AS INT64) AS reporte_fuente,
    JSON_EXTRACT_SCALAR(payload,
        "$['short_uid']") AS reporte_short_uid,
    -- UNIQUE ID Aleatorio que pides que genere. Solo te lo va a pedir si el usuario le da a esta.

    JSON_EXTRACT_SCALAR(payload,
        "$['cod_monitor']") AS meta_codigo_monitor,

    -- DATETIME(TIMESTAMP_MILLIS( SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
    --     "$['CreationDate']") AS INT64) ), 'America/Caracas'
    --     ) AS reporte_creation_date,
    -- DATETIME(TIMESTAMP_MILLIS( SAFE_CAST( JSON_EXTRACT_SCALAR(payload,
    --     "$['edit_date']") AS INT64) ), 'America/Caracas'
    --     ) AS reporte_edit_date
        
  FROM
    `event-pipeline.angostura.raw_events`
  WHERE
    event = 'agua_3'

--   ORDER BY reporte_edit_date DESC

  
  ),
  parroquias_ref AS (
    -- this acts as a reference table for the parroquia_id and the parroquia_name
  SELECT
    DISTINCT cod AS parroquia_code,
    parroquia AS parroquia_name
  FROM
    `event-pipeline.angostura_dev.monitor_ciudad_geo_demo_dev` ),
  municipios_ref AS (
    -- this acts as a reference table for the municipio_id and the municipio_name
  SELECT
    DISTINCT muncod AS municipio_code,
    nom_mun AS municipio_name
  FROM
    `event-pipeline.angostura_dev.monitor_ciudad_geo_demo_dev` ) /* ************************************


**************************************** */
SELECT
  monitor_ciudad_c_parroq.loc_entidad,
  municipios_ref.municipio_name AS loc_municipio,
  monitor_ciudad_c_parroq.loc_parroquia,
  monitor_ciudad_c_parroq.loc_sector,
  monitor_ciudad_c_parroq.loc_coordx_temp,
  monitor_ciudad_c_parroq.loc_coordy_temp,
  monitor_ciudad_c_parroq.loc_coord_temp,
  monitor_ciudad_c_parroq.agua_hidrologica,
  monitor_ciudad_c_parroq.reporte_semana_nro,
  monitor_ciudad_c_parroq._dia_semana_inicio,
  monitor_ciudad_c_parroq.reporte_semana_inicio,
  monitor_ciudad_c_parroq.reporte_semana_fin,
  monitor_ciudad_c_parroq.agua_suministro_semanal,
  monitor_ciudad_c_parroq.agua_interrupcion_dias,
  monitor_ciudad_c_parroq.agua_recibida_dias,
  monitor_ciudad_c_parroq.agua_horas_lunes,
  monitor_ciudad_c_parroq.agua_horas_martes,
  monitor_ciudad_c_parroq.agua_horas_miercoles,
  monitor_ciudad_c_parroq.agua_horas_jueves,
  monitor_ciudad_c_parroq.agua_horas_viernes,
  monitor_ciudad_c_parroq.agua_horas_sabado,
  monitor_ciudad_c_parroq.agua_horas_domingo,
  monitor_ciudad_c_parroq.agua_horas_dialaboral,
  monitor_ciudad_c_parroq.agua_horas_finsemana,
  monitor_ciudad_c_parroq.agua_horas_semana,
  monitor_ciudad_c_parroq.agua_horas_ciclo1,
  monitor_ciudad_c_parroq.agua_horas_ciclo2,
  monitor_ciudad_c_parroq.agua_sin_servicio_dias,
  monitor_ciudad_c_parroq.agua_frecuencia,
  monitor_ciudad_c_parroq.agua_otra_frecuencia_semanas,
  monitor_ciudad_c_parroq.agua_acceso,
  monitor_ciudad_c_parroq.agua_almacenada_dias,
  monitor_ciudad_c_parroq.agua_tanque_llenado,
  monitor_ciudad_c_parroq.agua_tanque_recoleccion_horas,
  monitor_ciudad_c_parroq.agua_cisterna_costo,
  monitor_ciudad_c_parroq.agua_bool_cisterna_en_tobos,
  monitor_ciudad_c_parroq.agua_bool_acarreo_pago,
  monitor_ciudad_c_parroq.agua_acarreo_costo,
  monitor_ciudad_c_parroq.agua_acarreo_horas,
  monitor_ciudad_c_parroq.agua_bool_mantener_higiene,
  monitor_ciudad_c_parroq.agua_comentario,
  monitor_ciudad_c_parroq.agua_bool_calidad,
  monitor_ciudad_c_parroq.agua_calidad_caract,
  monitor_ciudad_c_parroq.agua_potabilizacion,
  monitor_ciudad_c_parroq.agua_potabilizacion_otro,
  monitor_ciudad_c_parroq.agua_costo_botella,
  monitor_ciudad_c_parroq.elec_bool_interrupcion,
--   monitor_ciudad_c_parroq.elec_interrupcion_dias, -- Jesus posiblemente la elimine
  monitor_ciudad_c_parroq.elec_horas_lunes,
  monitor_ciudad_c_parroq.elec_horas_martes,
  monitor_ciudad_c_parroq.elec_horas_miercoles,
  monitor_ciudad_c_parroq.elec_horas_jueves,
  monitor_ciudad_c_parroq.elec_horas_viernes,
  monitor_ciudad_c_parroq.elec_horas_sabado,
  monitor_ciudad_c_parroq.elec_horas_domingo,
  monitor_ciudad_c_parroq.elec_horas_dialaboral,
  monitor_ciudad_c_parroq.elec_horas_finsemana,
  monitor_ciudad_c_parroq.elec_horas_semana,
  monitor_ciudad_c_parroq.elec_bool_danos_col,
  monitor_ciudad_c_parroq.elec_comentario,
  monitor_ciudad_c_parroq.gas_fuente,
  monitor_ciudad_c_parroq.gas_conseguir_bombona_dias,
  monitor_ciudad_c_parroq.gas_tipo_bombona,
  monitor_ciudad_c_parroq.gas_costo_bombona,
  monitor_ciudad_c_parroq.gas_bool_camion,
  monitor_ciudad_c_parroq.gas_comentario,
  monitor_ciudad_c_parroq.meta_codigo_monitor,
  monitor_ciudad_c_parroq.loc_habitantes_parroquia,
  monitor_ciudad_c_parroq.reporte_hora_inicio,
  monitor_ciudad_c_parroq.reporte_hora_fin,
  monitor_ciudad_c_parroq.reporte_tiempo_llenado,
  monitor_ciudad_c_parroq.reporte_fuente,
  monitor_ciudad_c_parroq.reporte_short_uid,
--   monitor_ciudad_c_parroq.reporte_creation_date,
--   monitor_ciudad_c_parroq.reporte_edit_date,
--   monitor_ciudad_c_parroq.global_id
FROM (
    -- query that replaces parroquia codes for parroquia names
  SELECT
    -- parroquias_ref.parroquia_code AS p_code_ref,
    -- monitor_ciudad_agua2.loc_municipio_id AS m_code_mc,
    parroquias_ref.parroquia_name as loc_parroquia,
    monitor_ciudad_agua2.loc_parroquia_id,
    monitor_ciudad_agua2.*
  FROM
    monitor_ciudad_agua2
  LEFT JOIN
    parroquias_ref
  ON
    monitor_ciudad_agua2.loc_parroquia_id = parroquias_ref.parroquia_code ) AS monitor_ciudad_c_parroq
LEFT JOIN
  municipios_ref
ON
  monitor_ciudad_c_parroq.loc_municipio_id = municipios_ref.municipio_code;