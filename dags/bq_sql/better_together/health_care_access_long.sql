WITH union_agg AS (
    SELECT submission_id
        , 'avg_days_invested_on_medication_search' AS question_name
        , CAST(avg_days_invested_on_medication_search AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'did_receive_medical_attention_during_visit' AS question_name
        , CAST(did_receive_medical_attention_during_visit AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'supply_stores_visited' AS question_name
        , CAST(supply_stores_visited AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'other_mode_of_transport_for_medication' AS question_name
        , CAST(other_mode_of_transport_for_medication AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'currency_for_buying_medications' AS question_name
        , CAST(currency_for_buying_medications AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'reason_for_not_getting_medication' AS question_name
        , CAST(reason_for_not_getting_medication AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'other_reason_for_3months_medications_supply' AS question_name
        , CAST(other_reason_for_3months_medications_supply AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'monthly_hypertension_medicine_units_needed' AS question_name
        , CAST(monthly_hypertension_medicine_units_needed AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'someone_has_chronic_health_conditions_requiring_medicine' AS question_name
        , CAST(someone_has_chronic_health_conditions_requiring_medicine AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'avg_monthly_spent_on_medications_or_attention' AS question_name
        , CAST(avg_monthly_spent_on_medications_or_attention AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'has_limit_to_medications_amount' AS question_name
        , CAST(has_limit_to_medications_amount AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'medication_source' AS question_name
        , CAST(medication_source AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'was_asked_to_take_medication' AS question_name
        , CAST(was_asked_to_take_medication AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'takes_medicine_for_chronic_health_conditions' AS question_name
        , CAST(takes_medicine_for_chronic_health_conditions AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'amount_increse_in_medications_price' AS question_name
        , CAST(amount_increse_in_medications_price AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'who_attended_visit' AS question_name
        , CAST(who_attended_visit AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'received_prescription' AS question_name
        , CAST(received_prescription AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'regular_access_to_medications' AS question_name
        , CAST(regular_access_to_medications AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'medications_supply_length' AS question_name
        , CAST(medications_supply_length AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'other_reason_for_no_attention_during_visit' AS question_name
        , CAST(other_reason_for_no_attention_during_visit AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'type_of_chronic_health_conditions_requiring_medicine' AS question_name
        , CAST(type_of_chronic_health_conditions_requiring_medicine AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'reason_for_not_taking_medication' AS question_name
        , CAST(reason_for_not_taking_medication AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'visited_center_last_3months' AS question_name
        , CAST(visited_center_last_3months AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'type_of_visited_center' AS question_name
        , CAST(type_of_visited_center AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'noticed_increse_in_medications_price' AS question_name
        , CAST(noticed_increse_in_medications_price AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'mode_of_transport_for_medication' AS question_name
        , CAST(mode_of_transport_for_medication AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'reason_for_3months_medications_supply' AS question_name
        , CAST(reason_for_3months_medications_supply AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'reason_for_no_attention_during_visit' AS question_name
        , CAST(reason_for_no_attention_during_visit AS STRING) AS answer
    FROM `{{ params.src_table }}`
)
SELECT tbl.*, md.language_select, md.question_proper, md.question_type
FROM union_agg as tbl
JOIN `event-pipeline.better_together.survey_question_metadata` AS md
  ON md.question_name = tbl.question_name
