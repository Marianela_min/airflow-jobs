WITH union_agg AS (
    SELECT submission_id
        , 'cereals_plantains_tubers_7days' AS question_name
        , CAST(cereals_plantains_tubers_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'vegs_7days' AS question_name
        , CAST(vegs_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'fruits_7days' AS question_name
        , CAST(fruits_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'meat_eggs_seafood_7days' AS question_name
        , CAST(meat_eggs_seafood_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'grains_7days' AS question_name
        , CAST(grains_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'dairy_7days' AS question_name
        , CAST(dairy_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'oils_fats_7days' AS question_name
        , CAST(oils_fats_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'sugars_7days' AS question_name
        , CAST(sugars_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'cheap_food_7days' AS question_name
        , CAST(cheap_food_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'skip_food_7days' AS question_name
        , CAST(skip_food_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'portion_reduction_7days' AS question_name
        , CAST(portion_reduction_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'adult_reduction_7days' AS question_name
        , CAST(adult_reduction_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'adult_reduction_copy_7days' AS question_name
        , CAST(adult_reduction_copy_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'non_adult_sent_to_eat_somewhere_else_7days' AS question_name
        , CAST(non_adult_sent_to_eat_somewhere_else_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'adult_sent_to_eat_somewhere_else_7days' AS question_name
        , CAST(adult_sent_to_eat_somewhere_else_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'borrowed_food_7days' AS question_name
        , CAST(borrowed_food_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'help_from_others_in_country_7days' AS question_name
        , CAST(help_from_others_in_country_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'help_from_others_outside_country_7days' AS question_name
        , CAST(help_from_others_outside_country_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'spend_food_savings_7days' AS question_name
        , CAST(spend_food_savings_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'sell_or_exchange_property_7days' AS question_name
        , CAST(sell_or_exchange_property_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'work_in_exchange_for_food_7days' AS question_name
        , CAST(work_in_exchange_for_food_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'food_purchased_7days' AS question_name
        , CAST(food_purchased_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'food_purchased_from_7days' AS question_name
        , CAST(food_purchased_from_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'food_transferred_7days' AS question_name
        , CAST(food_transferred_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'avg_distance_traveled_for_food_7days' AS question_name
        , CAST(avg_distance_traveled_for_food_7days AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'head_household' AS question_name
        , CAST(head_household AS STRING) AS answer
    FROM `{{ params.src_table }}`
)
SELECT tbl.*, md.language_select, md.question_proper, md.question_type
FROM union_agg as tbl
JOIN `event-pipeline.better_together.survey_question_metadata` AS md
  ON md.question_name = tbl.question_name
