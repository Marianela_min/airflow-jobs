WITH vzla_loc AS ( -- use our small venezuela dump from Reddit's public fh-bigquery.geocode.201806_geolite2_latlon_redux
  SELECT *, ST_GEOGPOINT(lon,lat) latlon_geo
  FROM `event-pipeline.better_together_raw.201806_geolite2_latlon_redux_vzla`
), tbl_v1 AS ( -- generate BQ geolocations from response lat, lon
  SELECT *
    , ST_GEOGPOINT(observation_lon, observation_lat) AS geo_location
    , ST_ASTEXT(ST_GEOGPOINT(observation_lon, observation_lat)) AS geo_location_text
  FROM `{{ params.src_table }}`
), loc_mapping AS ( -- perform cross join and find reasonable close match (if any) of response location -> geolite entry
  SELECT geo_location_text
    , ARRAY_AGG(
         STRUCT(city_name, subdivision_1_name, country_name)
         ORDER BY ST_DISTANCE(geo_location, latlon_geo) LIMIT 1
      )[SAFE_OFFSET(0)] loc
  FROM tbl_v1, vzla_loc
  WHERE ST_DWITHIN(geo_location, latlon_geo, 100000)
  GROUP BY 1
),  tbl_v2 AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT submission_id
      , observation_id
      , submitted_time AS submitted_timestamp
      , DATE(submitted_time) AS submitted_date
      , TIME(submitted_time) AS submitted_time
      , form_id
      , form_version
      , form_name
      , form_tier
      , campaign_id
      , campaign_name
      , project_id
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , ethnicity
      , religion
      , batch_date
      , SPLIT(campaign_name, '-')[OFFSET(0)] AS campaign_state
      , rt.loc.city_name AS submitted_city
      , rt.loc.subdivision_1_name AS submitted_state
      , CAST(have_you_been_to_a_health_center_in_the_last_3_months AS INT64) AS visited_center_last_3months
      , what_type_of_health_facility_did_you_visit AS type_of_visited_center
      , CAST(did_you_receive_medical_attention_at_the_health_clinic AS INT64) AS did_receive_medical_attention_during_visit
      , what_was_the_reason_you_did_not_receive_medical_attention AS reason_for_no_attention_during_visit
      , what_was_the_reason_you_did_not_receive_medical_attention_at_the_health_facility AS other_reason_for_no_attention_during_visit
      , who_came_to_the_health_facility_visit AS who_attended_visit
      , CAST(to_be_cared_for_or_given_the_treatment_you_needed_were_you_asked_to_take_any_medication_or_supplies AS INT64) AS was_asked_to_take_medication
      , CAST(did_you_receive_a_prescription_for_any_medication_to_take_at_home AS INT64) AS received_prescription
      , how_did_you_get_the_medication AS medication_source
      , why_were_you_not_able_to_get_the_medication AS reason_for_not_getting_medication
      , CAST(how_many_pharmacies_or_medical_supply_stores_did_you_have_to_visit_to_look_for_the_required_medications AS INT64) AS supply_stores_visited
      , CAST(can_you_indicate_the_average_number_of_day_invested_to_get_the_medicine_you_need AS INT64) AS avg_days_invested_on_medication_search
      , how_did_you_go_to_get_your_medication AS mode_of_transport_for_medication
      , how_do_you_go_get_your_medication AS other_mode_of_transport_for_medication
      , CAST(do_you_or_someone_in_your_family_live_with_one_of_the_following_chronic_health_conditions_that_depends_on_taking_medicine_daily AS INT64) AS someone_has_chronic_health_conditions_requiring_medicine
      , please_indicate_which_chronic_health_condition_s AS type_of_chronic_health_conditions_requiring_medicine
      , CAST(do_you_or_your_family_member_take_any_sort_of_daily_medication_to_manage_this_disease AS INT64) AS takes_medicine_for_chronic_health_conditions
      , CAST(how_many_units_of_anti_hypertension_medicine_do_you_need_in_your_house_every_months AS INT64) AS monthly_hypertension_medicine_units_needed
      , CAST(is_there_a_limit_to_the_amount_of_medications_that_you_can_buy_are_available AS INT64) AS has_limit_to_medications_amount
      , do_you_have_a_supply_of_medications_for_your_or_your_family_member_s_condition_s_for AS medications_supply_length
      , why_do_you_have_reserve_of_medication_of_more_than_3_months AS reason_for_3months_medications_supply
      , _29_why_do_you_have_reserve_of_medication_of_more_than_3_months AS other_reason_for_3months_medications_supply
      , CAST(do_you_have_regular_access_to_medications_for_your_family AS INT64) AS regular_access_to_medications
      , for_what_reason_do_you_not_regularly_take_the_medication AS reason_for_not_taking_medication
      , with_what_currency_do_you_typically_buy_medications AS currency_for_buying_medications
      , how_much_to_you_spend_monthly_on_average_on_medication_or_medical_attention_for_this_chronic_medical_condition_s AS avg_monthly_spent_on_medications_or_attention
      , CAST(between_buying_medications_have_you_noticed_an_increase_in_price_of_medications AS INT64) AS noticed_increse_in_medications_price
      , how_much_has_the_price_of_medications_increased AS amount_increse_in_medications_price
  FROM tbl_v1 AS lt
  LEFT JOIN loc_mapping as rt ON lt.geo_location_text = rt.geo_location_text
)
SELECT *
FROM tbl_v2
