WITH agg_tbl AS (
  SELECT 'daily_food' AS datasource
    , submitted_date
    , submitted_state
    , submitted_city
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , COUNT(*) AS count
  FROM `{{ params.daily_food_table }}`
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
  UNION ALL
  SELECT 'weekly_food' AS datasource
    , submitted_date
    , submitted_state
    , submitted_city
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , COUNT(*) AS count
  FROM `{{ params.weekly_food_table }}`
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
  UNION ALL
  SELECT 'monthly_health' AS datasource
    , submitted_date
    , submitted_state
    , submitted_city
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , COUNT(*) AS count
  FROM `{{ params.monthly_health_table }}`
  GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
SELECT *
FROM agg_tbl
