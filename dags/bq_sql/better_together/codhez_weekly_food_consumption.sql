WITH vzla_loc AS ( -- use our small venezuela dump from Reddit's public fh-bigquery.geocode.201806_geolite2_latlon_redux
  SELECT *, ST_GEOGPOINT(lon,lat) latlon_geo
  FROM `event-pipeline.better_together_raw.201806_geolite2_latlon_redux_vzla`
), tbl_v1 AS ( -- generate BQ geolocations from response lat, lon
  SELECT *
    , ST_GEOGPOINT(observation_lon, observation_lat) AS geo_location
    , ST_ASTEXT(ST_GEOGPOINT(observation_lon, observation_lat)) AS geo_location_text
  FROM `{{ params.src_table }}`
), loc_mapping AS ( -- perform cross join and find reasonable close match (if any) of response location -> geolite entry
  SELECT geo_location_text
    , ARRAY_AGG(
         STRUCT(city_name, subdivision_1_name, country_name)
         ORDER BY ST_DISTANCE(geo_location, latlon_geo) LIMIT 1
      )[SAFE_OFFSET(0)] loc
  FROM tbl_v1, vzla_loc
  WHERE ST_DWITHIN(geo_location, latlon_geo, 100000)
  GROUP BY 1
), tbl_v2 AS (  -- append location to table
  SELECT lt.*
      , rt.loc.city_name AS submitted_city
      , rt.loc.subdivision_1_name AS submitted_state
  FROM tbl_v1 AS lt
  LEFT JOIN loc_mapping as rt ON lt.geo_location_text = rt.geo_location_text
), tbl_v3 AS ( -- rename long column names for easier handling
  SELECT *
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_cereals_plantains_or_tubers AS cereals_plantains_tubers_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_vegetables AS vegs_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_fruits AS fruits_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_meat_eggs_fish_or_seafood AS meat_eggs_seafood_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_grains AS grains_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_diary_products AS dairy_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_oil_or_fats AS oils_fats_7days
    , in_your_home_how_many_day_in_the_last_week_have_you_consumed_sugar AS sugars_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_consume_food_that_was_cheaper_or_less_preferable AS cheap_food_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_skip_breakfast_lunch_or_dinner AS skip_food_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_decrease_the_portion_size_of_meals AS portion_reduction_7days
    , in_the_last_7_days_how_frequently_have_adults_had_to_reduce_their_consumption_so_children_or_adolescents_could_eat AS adult_reduction_7days
    , in_the_last_7_days_how_frequently_have_adults_had_to_reduce_their_consumption_so_children_or_adolescents_could_eat_copy AS adult_reduction_copy_7days
    , in_the_last_7_days_how_frequently_have_you_sent_a_child_or_adolescent_who_lives_in_your_house_to_eat_somewhere_else AS non_adult_sent_to_eat_somewhere_else_7days
    , in_the_last_7_days_how_frequently_have_you_sent_an_adult_who_lives_in_your_house_to_eat_somewhere_else AS adult_sent_to_eat_somewhere_else_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_borrow_food AS borrowed_food_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_eat_depending_on_the_help_of_neighbors_friends_or_relatives_who_live_TRUNCATED AS help_from_others_in_country_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_eat_depending_on_the_help_of_relatives_who_live_outside_of_the_country AS help_from_others_outside_country_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_spend_food_savings AS spend_food_savings_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_sell_or_exchange_personal_property_in_order_to_buy_foods AS sell_or_exchange_property_7days
    , in_the_last_7_days_how_frequently_have_you_had_to_work_in_exchange_for_food AS work_in_exchange_for_food_7days
    , in_the_last_7_days_how_many_times_have_the_foods_consumed_in_your_home_been_purchased AS food_purchased_7days
    , in_the_last_7_days_where_have_you_bought_the_food_that_you_eat_in_your_household AS food_purchased_from_7days
    , in_the_last_7_days_how_have_you_transferred_food_you_ve_bought_to_your_house AS food_transferred_7days
    , taking_the_last_7_days_as_reference_how_much_distance_is_traveled_on_average_each_time_you_need_to_buy_food_to_be_cons_TRUNCATED AS avg_distance_traveled_for_food_7days
    , are_you_the_head_of_household AS head_household
  FROM tbl_v2
), tbl_v4 AS ( -- grab subset of desired columns, clean some of them, and generate new simple ones
  SELECT submission_id
    , observation_id
    , submitted_time AS submitted_timestamp
    , DATE(submitted_time) AS submitted_date
    , TIME(submitted_time) AS submitted_time
    , form_id
    , form_version
    , form_name
    , form_tier
    , campaign_id
    , campaign_name
    , project_id
    , gender
    , age
    , geography
    , financial_situation
    , education
    , employment_status
    , ethnicity
    , religion
    , batch_date
    , SPLIT(campaign_name, '-')[OFFSET(0)] AS campaign_state
    , submitted_city
    , submitted_state
    , CASE WHEN cereals_plantains_tubers_7days = 'Never' THEN 0 ELSE CAST(SPLIT(cereals_plantains_tubers_7days, ' ')[OFFSET(0)] AS INT64) END AS cereals_plantains_tubers_7days
    , CASE WHEN vegs_7days = 'Never' THEN 0 ELSE CAST(SPLIT(vegs_7days, ' ')[OFFSET(0)] AS INT64) END AS vegs_7days
    , CASE WHEN fruits_7days = 'Never' THEN 0 ELSE CAST(SPLIT(fruits_7days, ' ')[OFFSET(0)] AS INT64) END AS fruits_7days
    , CASE WHEN meat_eggs_seafood_7days = 'Never' THEN 0 ELSE CAST(SPLIT(meat_eggs_seafood_7days, ' ')[OFFSET(0)] AS INT64) END AS meat_eggs_seafood_7days
    , CASE WHEN grains_7days = 'Never' THEN 0 ELSE CAST(SPLIT(grains_7days, ' ')[OFFSET(0)] AS INT64) END AS grains_7days
    , CASE WHEN dairy_7days = 'Never' THEN 0 ELSE CAST(SPLIT(dairy_7days, ' ')[OFFSET(0)] AS INT64) END AS dairy_7days
    , CASE WHEN oils_fats_7days = 'Never' THEN 0 ELSE CAST(SPLIT(oils_fats_7days, ' ')[OFFSET(0)] AS INT64) END AS oils_fats_7days
    , CASE WHEN sugars_7days = 'Never' THEN 0 ELSE CAST(SPLIT(sugars_7days, ' ')[OFFSET(0)] AS INT64) END AS sugars_7days
    , CASE WHEN cheap_food_7days = 'Never' THEN 0 ELSE CAST(SPLIT(cheap_food_7days, ' ')[OFFSET(0)] AS INT64) END AS cheap_food_7days
    , CASE WHEN skip_food_7days = 'Never' THEN 0 ELSE CAST(SPLIT(skip_food_7days, ' ')[OFFSET(0)] AS INT64) END AS skip_food_7days
    , CASE WHEN portion_reduction_7days = 'Never' THEN 0 ELSE CAST(SPLIT(portion_reduction_7days, ' ')[OFFSET(0)] AS INT64) END AS portion_reduction_7days
    , CASE WHEN adult_reduction_7days = 'Never' THEN 0 ELSE CAST(SPLIT(adult_reduction_7days, ' ')[OFFSET(0)] AS INT64) END AS adult_reduction_7days
    , CASE WHEN adult_reduction_copy_7days = 'Never' THEN 0 ELSE CAST(SPLIT(adult_reduction_copy_7days, ' ')[OFFSET(0)] AS INT64) END AS adult_reduction_copy_7days
    , CASE WHEN non_adult_sent_to_eat_somewhere_else_7days = 'Never' THEN 0 ELSE CAST(SPLIT(non_adult_sent_to_eat_somewhere_else_7days, ' ')[OFFSET(0)] AS INT64) END AS non_adult_sent_to_eat_somewhere_else_7days
    , CASE WHEN adult_sent_to_eat_somewhere_else_7days = 'Never' THEN 0 ELSE CAST(SPLIT(adult_sent_to_eat_somewhere_else_7days, ' ')[OFFSET(0)] AS INT64) END AS adult_sent_to_eat_somewhere_else_7days
    , CASE WHEN borrowed_food_7days = 'Never' THEN 0 ELSE CAST(SPLIT(borrowed_food_7days, ' ')[OFFSET(0)] AS INT64) END AS borrowed_food_7days
    , CASE WHEN help_from_others_in_country_7days = 'Never' THEN 0 ELSE CAST(SPLIT(help_from_others_in_country_7days, ' ')[OFFSET(0)] AS INT64) END AS help_from_others_in_country_7days
    , CASE WHEN help_from_others_outside_country_7days = 'Never' THEN 0 ELSE CAST(SPLIT(help_from_others_outside_country_7days, ' ')[OFFSET(0)] AS INT64) END AS help_from_others_outside_country_7days
    , CASE WHEN spend_food_savings_7days = 'Never' THEN 0 ELSE CAST(SPLIT(spend_food_savings_7days, ' ')[OFFSET(0)] AS INT64) END AS spend_food_savings_7days
    , CASE WHEN sell_or_exchange_property_7days = 'Never' THEN 0 ELSE CAST(SPLIT(sell_or_exchange_property_7days, ' ')[OFFSET(0)] AS INT64) END AS sell_or_exchange_property_7days
    , CASE WHEN work_in_exchange_for_food_7days = 'Never' THEN 0 ELSE CAST(SPLIT(work_in_exchange_for_food_7days, ' ')[OFFSET(0)] AS INT64) END AS work_in_exchange_for_food_7days
    , CASE WHEN food_purchased_7days = 'Never' THEN 0 ELSE CAST(SPLIT(food_purchased_7days, ' ')[OFFSET(0)] AS INT64) END AS food_purchased_7days
    , food_purchased_from_7days
    , food_transferred_7days
    , avg_distance_traveled_for_food_7days
    , head_household
  FROM tbl_v3
)
SELECT *
FROM tbl_v4
