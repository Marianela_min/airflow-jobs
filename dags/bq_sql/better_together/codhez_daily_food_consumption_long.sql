WITH union_agg AS (
    SELECT submission_id
        , 'cereal_24hrs' AS question_name
        , CAST(cereal_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'plantains_24hrs' AS question_name
        , CAST(plantains_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'vegs_24hrs' AS question_name
        , CAST(vegs_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'fruits_24hrs' AS question_name
        , CAST(fruits_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'meat_24hrs' AS question_name
        , CAST(meat_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'eggs_24hrs' AS question_name
        , CAST(eggs_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'fish_24hrs' AS question_name
        , CAST(fish_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'grains_24hrs' AS question_name
        , CAST(grains_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'dairy_24hrs' AS question_name
        , CAST(dairy_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'oils_fats_24hrs' AS question_name
        , CAST(oils_fats_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'sugars_24hrs' AS question_name
        , CAST(sugars_24hrs AS STRING) AS answer
    FROM `{{ params.src_table }}`
    UNION ALL
    SELECT submission_id
        , 'head_household' AS question_name
        , CAST(head_household AS STRING) AS answer
    FROM `{{ params.src_table }}`
)
SELECT tbl.*, md.language_select, md.question_proper, md.question_type
FROM union_agg as tbl
JOIN `event-pipeline.better_together.survey_question_metadata` AS md
  ON md.question_name = tbl.question_name
