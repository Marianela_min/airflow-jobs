WITH vzla_loc AS ( -- use our small venezuela dump from Reddit's public fh-bigquery.geocode.201806_geolite2_latlon_redux
  SELECT *, ST_GEOGPOINT(lon,lat) latlon_geo
  FROM `event-pipeline.better_together_raw.201806_geolite2_latlon_redux_vzla`
), tbl_v1 AS ( -- generate BQ geolocations from response lat, lon
  SELECT *
    , ST_GEOGPOINT(observation_lon, observation_lat) AS geo_location
    , ST_ASTEXT(ST_GEOGPOINT(observation_lon, observation_lat)) AS geo_location_text
  FROM `{{ params.src_table }}`
), loc_mapping AS ( -- perform cross join and find reasonable close match (if any) of response location -> geolite entry
  SELECT geo_location_text
    , ARRAY_AGG(
         STRUCT(city_name, subdivision_1_name, country_name)
         ORDER BY ST_DISTANCE(geo_location, latlon_geo) LIMIT 1
      )[SAFE_OFFSET(0)] loc
  FROM tbl_v1, vzla_loc
  WHERE ST_DWITHIN(geo_location, latlon_geo, 100000)
  GROUP BY 1
), tbl_v2 AS (  -- append location, grab subset of desired columns, and generate new simple ones
  SELECT submission_id
      , observation_id
      , submitted_time AS submitted_timestamp
      , DATE(submitted_time) AS submitted_date
      , TIME(submitted_time) AS submitted_time
      , form_id
      , form_version
      , form_name
      , form_tier
      , campaign_id
      , campaign_name
      , project_id
      , gender
      , age
      , geography
      , financial_situation
      , education
      , employment_status
      , ethnicity
      , religion
      , batch_date
      , SPLIT(campaign_name, '-')[OFFSET(0)] AS campaign_state
      , rt.loc.city_name AS submitted_city
      , rt.loc.subdivision_1_name AS submitted_state
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_cereals AS INT64) AS cereal_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_plantains_or_tubers AS INT64) AS plantains_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_vegetables AS INT64) AS vegs_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_fruits AS INT64) AS fruits_24hrs
      , CAST(in_your_home_the_last_24_hours_did_you_eat_any_meat AS INT64) AS meat_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_eggs AS INT64) AS eggs_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_fish_or_seafood AS INT64) AS fish_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_grains AS INT64) AS grains_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_dairy_products AS INT64) AS dairy_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_oils_or_fats AS INT64) AS oils_fats_24hrs
      , CAST(in_your_home_in_the_last_24_hours_did_you_eat_any_sugar AS INT64) AS sugars_24hrs
      , are_you_the_head_of_household AS head_household
  FROM tbl_v1 AS lt
  LEFT JOIN loc_mapping as rt ON lt.geo_location_text = rt.geo_location_text
), tbl_v3 AS ( -- run window aggregations
  SELECT *
    , SUM(cereal_24hrs) OVER (ORDER BY submitted_timestamp) AS cereal_24hrs_cumsum
    , SUM(plantains_24hrs) OVER (ORDER BY submitted_timestamp) AS plantains_24hrs_cumsum
    , SUM(vegs_24hrs) OVER (ORDER BY submitted_timestamp) AS vegs_24hrs_cumsum
    , SUM(fruits_24hrs) OVER (ORDER BY submitted_timestamp) AS fruits_24hrs_cumsum
    , SUM(meat_24hrs) OVER (ORDER BY submitted_timestamp) AS meat_24hrs_cumsum
    , SUM(eggs_24hrs) OVER (ORDER BY submitted_timestamp) AS eggs_24hrs_cumsum
    , SUM(fish_24hrs) OVER (ORDER BY submitted_timestamp) AS fish_24hrs_cumsum
    , SUM(grains_24hrs) OVER (ORDER BY submitted_timestamp) AS grains_24hrs_cumsum
    , SUM(dairy_24hrs) OVER (ORDER BY submitted_timestamp) AS dairy_24hrs_cumsum
    , SUM(oils_fats_24hrs) OVER (ORDER BY submitted_timestamp) AS oils_fats_24hrs_cumsum
    , SUM(sugars_24hrs) OVER (ORDER BY submitted_timestamp) AS sugars_24hrs_cumsum
  FROM tbl_v2
)
SELECT *
FROM tbl_v3
