-- This script transforms events from the Bot tracking symptomps of people in
-- Venezuela which publishes the answers to the proxy
--
-- An example payload can be found at covid_bot_diagnostic_payload_example.json
SELECT DISTINCT
CASE event
  WHEN "covid_bot_diagnostic" THEN "Venezuela"
  WHEN "covid_bot_diagnostic_peru" THEN "Peru"
  WHEN "covid_bot_diagnostic_colombia" THEN "Colombia"
END AS country,
  SAFE_CAST(created_at AS TIMESTAMP) AS timestamp,
  JSON_EXTRACT_SCALAR(payload, "$.estado") AS state,
  JSON_EXTRACT_SCALAR(payload, "$.ciudad") AS city,
  JSON_EXTRACT_SCALAR(payload, "$.municipio") AS municipality,
  JSON_EXTRACT_SCALAR(payload, "$.parroquias") AS parish,
  JSON_EXTRACT_SCALAR(payload, "$.contacto-covid19") AS covid_contact,
  JSON_EXTRACT_SCALAR(payload, "$.mayor-65") AS older_65,
  JSON_EXTRACT_SCALAR(payload, "$.fiebre") AS fever,
  JSON_EXTRACT_SCALAR(payload, "$.tos") AS cough,
  JSON_EXTRACT_SCALAR(payload, "$.dificultad-respirar") AS respiratory_difficulty,
  JSON_EXTRACT_SCALAR(payload, "$.viajes-internacionales") AS international_travel,
  JSON_EXTRACT_SCALAR(payload, "$.inmunidad") AS immunity,
  JSON_EXTRACT_SCALAR(payload, "$.med-inmune") AS immunity_drug,
  JSON_EXTRACT_SCALAR(payload, "$.historia-pulmon") AS lung_history,
  JSON_EXTRACT_SCALAR(payload, "$.mialgia") AS myalgia,
  JSON_EXTRACT_SCALAR(payload, "$.artralgia") AS arthralgia,
  JSON_EXTRACT_SCALAR(payload, "$.rinorrea") AS rhinorrhea,
  JSON_EXTRACT_SCALAR(payload, "$.escalofrios") AS chills,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, "$.riesgo_medico") AS NUMERIC) AS risk_medical,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, "$.riesgo_covid") AS NUMERIC) AS risk_covid,
  SAFE_CAST(JSON_EXTRACT_SCALAR(payload, "$.riesgo_total") AS NUMERIC) AS risk_total,
  JSON_EXTRACT_SCALAR(payload, "$.user['source']") AS user_source,
  JSON_EXTRACT_SCALAR(payload, "$.user['user_id']") AS user_id,
  JSON_EXTRACT_SCALAR(payload, "$.codigo_estado") AS state_code,
  JSON_EXTRACT_SCALAR(payload, "$.codigo_municipio") AS municipality_code,
  JSON_EXTRACT_SCALAR(payload, "$.codigo_parroquia") AS parish_code,
FROM `{{ params.raw_events_table }}`
WHERE event IN ("covid_bot_diagnostic_peru", "covid_bot_diagnostic_colombia", "covid_bot_diagnostic")
