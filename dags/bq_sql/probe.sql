/*
`probe` is a table that is going to be used by the `probe-analyzer` tool to
enhance our monitoring to ensure our airflow DAGs are working as expecteds.

`probe-analyzer` reads the data from the `probe` table, it expects that data
has a timestapm
*/
SELECT
  CAST(JSON_EXTRACT_SCALAR(payload, '$.timestamp') AS INT64) as timestamp
FROM
  `{{ params.raw_events_table }}`
WHERE event = 'probe';
