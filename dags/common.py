import json
from datetime import datetime, timedelta
import os

# GCP_CONN_ID is the identifier to the production service account
GCP_CONN_ID = 'gcp_production'

# DEFAULT_DAGS_FOLDER is the default folder in which DAGs are expected to be
DEFAULT_DAGS_FOLDER = '/opt/airflow/dags'

def get_dag_default_args(**kargs):
    """Get general sane DAG default configuration values."""
    default_args = {
        'owner': 'eng@codeforvenezuela.org',
        'depends_on_past': False,
        'start_date': datetime(2019, 12, 1),
        'email': ['eng@codeforvenezuela.com'],
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5),
        'dataflow_default_options': {
            'project': 'event-pipeline',
            'zone': 'us-central1-a',
            'stagingLocation': 'gs://angostura-dev/dataflow-staging-tmp-location/staging/',
            'gcpTempLocation': 'gs://angostura-dev/dataflow-staging-tmp-location/tmp/'
        }
    }
    default_args.update(kargs)
    return default_args


def get_dags_folder() -> str:
    dags_folder = os.getenv('AIRFLOW__CORE__DAGS_FOLDER', DEFAULT_DAGS_FOLDER)
    return os.path.join(dags_folder, 'dags') if os.environ["APP_NAMESPACE"] == 'prod' else dags_folder


def load_config():
    """Loads the json configuration for the active app namespace"""
    dags_folder = get_dags_folder()
    conf_filepath = os.path.join(
        dags_folder, 'config', f'{os.environ["APP_NAMESPACE"]}.json')
    with open(conf_filepath) as j_file:
        config = json.load(j_file)
    return config


def read_local_file(filename: str) -> str:
    """Reads a file which can be either an absolute path or a relative path from the DAGs folder"""
    dags_folder = get_dags_folder()
    filename = filename if filename.startswith('/') else os.path.join(dags_folder, filename)
    with open(filename) as f:
        return f.read()
