import time

from airflow.contrib.hooks.gcp_dataflow_hook import DataFlowHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults


class DataflowDrainOperator(BaseOperator):
    """Operator that given a Dataflow Job ID, it drains it (assuming that the job is a streaming one).

    This operator is useful when we want to limit the amount of time that a streaming job runs, effectively converting
    it into a batch job.

    :param job_name: name of the dataflow job that should be drained.
    :param gcp_conn_id: name of the GCP connection ID.
    :param dataflow_default_options: common options passed to most dataflow jobs. This is where we get project and zone.
        dataflow_default_options={
            'project': 'my-gcp-project',
            'zone': 'us-central1-f'
        }
    :param wait_time_s: amount of time to wait before draining in seconds.
    """
    _MAX_RETRIES = 2

    @apply_defaults
    def __init__(self, job_name, task_id, dataflow_default_options=None, gcp_conn_id='google_cloud_default',
                 wait_time_s=30, *args, **kwargs):
        super().__init__(task_id=task_id, *args, **kwargs)
        self.job_name = job_name
        self.gcp_conn_id = gcp_conn_id
        self.wait_time_s = wait_time_s
        self.dataflow_default_options = dataflow_default_options or {}

    def _get_job_id(self, service, job_name):
        """Gets a job_id out of a Dataflow Job Name only from active jobs."""
        job_name_prefix = self.job_name.replace('_', '-')
        request = service.projects().locations().jobs().list(
            projectId=self.dataflow_default_options.get('project'),
            location=self.dataflow_default_options.get('zone'),
            filter='ACTIVE',
        )
        response = request.execute(num_retries=self._MAX_RETRIES)
        for job in response['jobs']:
            # The Dataflow operator generates unique job names using the job_name as prefix.
            # We thus look for the first job name whose prefix matches the supplied job name.
            if not job['name'].startswith(job_name_prefix):
                continue

            return job['id']

        return None

    def execute(self, context):
        time.sleep(self.wait_time_s)

        dataflow_hook = DataFlowHook(gcp_conn_id=self.gcp_conn_id)
        service = dataflow_hook.get_conn()

        job_id = self._get_job_id(service, self.job_name)
        print(f"Got job ID: {job_id}")
        request = service.projects().locations().jobs().update(
            projectId=self.dataflow_default_options.get('project'),
            location=self.dataflow_default_options.get('zone'),
            jobId=job_id,
            body={
                'id': job_id,
                'requestedState': 'JOB_STATE_DRAINING',
            }
        )
        request.execute(num_retries=self._MAX_RETRIES)
