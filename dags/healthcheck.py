"""
DAG that sends every 5 minutes logs "airflow-is-ok"
"""
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta, datetime

import sys
import os
# TODO: this is an easy way to fix the import path on this file, should be removed once we figure out a way around.
sys.path.insert(0, f'{os.path.dirname(__file__)}/')

from common import get_dag_default_args

import logging

def task_fun(*args, **kwargs):
    logging.info('airflow-is-ok')

def generate_dag():
    dag = DAG(
        'healthcheck',
        schedule_interval='*/5 * * * *',
        catchup=False,
        max_active_runs=1,
        default_args=get_dag_default_args()
    )


    t1 = PythonOperator(
        task_id='logger',
        dag=dag,
        python_callable=task_fun
    )
    return dag

HEALTHCHECK_DAG = generate_dag()
