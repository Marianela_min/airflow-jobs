import json
import os
import sys

from airflow import DAG
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator

# TODO: this is an easy way to fix the import path on this file, should be removed once we figure out a way around.
sys.path.insert(0, f"{os.path.dirname(__file__)}/")
from common import get_dag_default_args, load_config, read_local_file, GCP_CONN_ID  # noqa


def generate_dag(dag_conf):
    """Returns a DAG definition based on the configuration given."""
    dag = DAG(
        dag_conf["name"],
        default_args=get_dag_default_args(),
        schedule_interval=dag_conf["cron_schedule"],
        catchup=False,
        max_active_runs=1,
        concurrency=3,
    )

    # Create tasks on first loop and cache them for later setting dependencies
    tasks_map = {}
    for job_conf in dag_conf["jobs"]:
        if job_conf["job_type"] == "gcs_to_bq":
            autodetect_schema = True  # allow schema auto-detection if no fixed schema is given
            schema_fields = None
            if job_conf.get("schema_filepath"):
                autodetect_schema = False
                schema_fields = json.loads(read_local_file(job_conf["schema_filepath"]))
            task = GoogleCloudStorageToBigQueryOperator(
                dag=dag,
                task_id=f'gcs_to_bq_{job_conf["job_id"]}',
                bucket=job_conf["src_bucket"],
                source_objects=job_conf["src_path_patterns"],
                destination_project_dataset_table=job_conf["dst_table"],
                write_disposition=job_conf["write_disposition"],
                skip_leading_rows=job_conf["skip_leading_rows"],
                bigquery_conn_id=GCP_CONN_ID,
                schema_fields=schema_fields,
                autodetect=autodetect_schema,
            )
        elif job_conf["job_type"] == "sql_transform":
            task = BigQueryOperator(
                dag=dag,
                task_id=f'sql_transform_{job_conf["job_id"]}',
                use_legacy_sql=False,
                sql=read_local_file(job_conf["sql_script"]),
                destination_dataset_table=job_conf["dst_table"],
                write_disposition=job_conf["write_disposition"],
                params=job_conf["sql_params"],
                bigquery_conn_id=GCP_CONN_ID,
            )
        tasks_map[job_conf["job_id"]] = task

    # With all tasks created, we can now chain dependencies
    for job_conf in dag_conf["jobs"]:
        for upstream_dep in job_conf.get("dependencies", []):
            tasks_map[job_conf["job_id"]] << tasks_map[upstream_dep]

    return dag


# Generate DAGs dynamically by adding them to globals
conf = load_config()
for dag_conf in conf["gcs_to_bq_dags"]:
    globals()[dag_conf["name"]] = generate_dag(dag_conf)
