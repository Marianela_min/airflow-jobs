package org.codeforvenezuela.jobs;

import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.Validation;
import org.apache.beam.sdk.values.PCollection;
import org.codeforvenezuela.jobs.functions.AngosturaEventTypeFn;
import org.codeforvenezuela.jobs.utils.ByEventTypeFileNaming;
import org.codeforvenezuela.jobs.utils.DurationUtils;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.StreamingOptions;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.Window;

/**
 * This pipeline ingests incoming data from a Cloud Pub/Sub topic and outputs the raw data into
 * windowed files at the specified output directory.
 *
 * <p>Example Usage:
 *
 * <pre>
 * mvn compile exec:java \
 *  -Dexec.mainClass=org.codeforvenezuela.jobs.PubsubToText \
 *  -Dexec.cleanupDaemonThreads=false \
 *  -Dexec.args=" \
 *     --project=event-pipeline \
 *     --runner=DirectRunner \
 *     --windowDuration=10m \
 *     --numShards=1 \
 *     --inputFilePattern=gs://angostura-dev/my_directory \
 *     --outputDirectory=/tmp/angostura/ \
 *     --outputFilenamePrefix=windowed-file \
 *     --outputFilenameSuffix=.json"
 * </pre>
 **/
public class PubsubToText {

  /**
   * Main entry point for executing the pipeline.
   *
   * @param args The command-line arguments to the pipeline.
   */
  public static void main(String[] args) {
    Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);

    options.setStreaming(true);
    run(options);
  }

  /**
   * Runs the pipeline with the supplied options.
   *
   * @param options The execution parameters to the pipeline.
   * @return The result of the pipeline execution.
   */
  public static PipelineResult run(Options options) {
    Pipeline pipeline = Pipeline.create(options);

    // Note that --inputFilePattern should only be used for testing (because we don't necessarily want to read
    // from PubSub when testing locally) or for running backfills and moving existing data to a different location.
    if (options.getInputFilePattern().isAccessible()) {
      writePartitionedByEventType(
          pipeline.apply("Read Input Files", TextIO.read().from(options.getInputFilePattern())),
          options);
    } else if (options.getInputSubscription().isAccessible()) {
      writePartitionedByEventType(
          pipeline.apply(
              "Read PubSub Events", PubsubIO.readStrings().fromSubscription(options.getInputSubscription())),
          options);
    } else {
      throw new IllegalArgumentException("Either inputFilePattern or inputTopic should be set");
    }

    return pipeline.run();
  }

  private static void writePartitionedByEventType(
      PCollection<String> inputEvents, Options options) {
    /*
     * Given a collection of Angostura events we will do the following:
     *   1) Window the messages into the given duration specified in the options.
     *   2) Write these into the given output directory while partitioning them by eventType/YYYY/MM/DD/
     */

    final ValueProvider<String> outputFilenamePrefix = options.getOutputFilenamePrefix();
    final ValueProvider<String> outputFilenameSuffix = options.getOutputFilenameSuffix();
    inputEvents
        .apply(
            options.getWindowDuration() + "Window",
            Window.into(FixedWindows.of(DurationUtils.parseDuration(options.getWindowDuration()))))
        .apply(
            FileIO.<String, String>writeDynamic()
                .by(new AngosturaEventTypeFn())
                .via(TextIO.sink())
                .withDestinationCoder(StringUtf8Coder.of())
                .withNaming(
                    eventType ->
                        ByEventTypeFileNaming.getNaming(
                            eventType, outputFilenamePrefix, outputFilenameSuffix))
                .withNumShards(options.getNumShards())
                .to(options.getOutputDirectory()));
  }

  /**
   * Options supported by the pipeline.
   *
   * <p>Inherits standard configuration options.
   */
  public interface Options extends PipelineOptions, StreamingOptions {
    @Description("The Cloud Pub/Sub subscription to read from.")
    ValueProvider<String> getInputSubscription();

    void setInputSubscription(ValueProvider<String> value);

    @Description("Input directory where to read data from. Preferred over inputTopic if set.")
    ValueProvider<String> getInputFilePattern();

    void setInputFilePattern(ValueProvider<String> value);

    @Description("The directory to output files to. Must end with a slash.")
    @Validation.Required
    ValueProvider<String> getOutputDirectory();

    void setOutputDirectory(ValueProvider<String> value);

    @Description("The filename prefix of the files to write to.")
    @Default.String("output")
    @Validation.Required
    ValueProvider<String> getOutputFilenamePrefix();

    void setOutputFilenamePrefix(ValueProvider<String> value);

    @Description("The suffix of the files to write.")
    @Default.String("")
    ValueProvider<String> getOutputFilenameSuffix();

    void setOutputFilenameSuffix(ValueProvider<String> value);

    @Description("The maximum number of output shards produced when writing.")
    @Default.Integer(1)
    Integer getNumShards();

    void setNumShards(Integer value);

    @Description(
        "The window duration in which data will be written. Defaults to 5m. "
            + "Allowed formats are: "
            + "Ns (for seconds, example: 5s), "
            + "Nm (for minutes, example: 12m), "
            + "Nh (for hours, example: 2h).")
    @Default.String("5m")
    String getWindowDuration();

    void setWindowDuration(String value);
  }
}
