#!/usr/bin/env bash
# Set up a forward connection into the Airflow web server Pod
set -euf -o pipefail

echo "Checking that dependencies are installed"
cmd=gcloud
[[ $(type -P "$cmd") ]] && echo "✔️ $cmd is in PATH! Thank you."  || { echo "❌ $cmd is NOT in PATH. Please install deps from dependencies in DEVELOPERS.md" 1>&2; exit 1; }
cmd=kubectl
[[ $(type -P "$cmd") ]] && echo "✔ ️$cmd is in PATH! Thank you."  || { echo "❌ $cmd is NOT in PATH. Please install deps from dependencies in DEVELOPERS.md" 1>&2; exit 1; }

echo "Authenticate with gcloud"
gcloud container clusters get-credentials dev-cluster --zone us-central1-a --project core-infra-256416

export PROD_WEB_POD=$(kubectl get pod --namespace airflow --selector="app=airflow,component=web,release=airflow" --output jsonpath='{.items[0].metadata.name}')
echo "Downloading credentials from $PROD_WEB_POD"
kubectl exec --namespace airflow "$PROD_WEB_POD" -c airflow-web -- cat /secrets/airflow-sa.json > .cronjob-sa.json

echo "Creating secret on local cluster from downloaded credentials"
kubectl config use-context docker-desktop
kubectl delete secret airflow-sa --ignore-not-found=true
kubectl create secret generic airflow-sa --from-file=.cronjob-sa.json
rm .cronjob-sa.json


