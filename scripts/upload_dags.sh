#!/usr/bin/env bash
# Uploads the local ./dags folder into the relevant Airflow PODs
set -euf -o pipefail

export SCHEDULER_POD=$(kubectl get pods --namespace default -l "component=scheduler,app=airflow" -o jsonpath="{.items[0].metadata.name}")
export WEB_POD=$(kubectl get pods --namespace default -l "component=web,app=airflow" -o jsonpath="{.items[0].metadata.name}")
export WORKER_POD=$(kubectl get pods --namespace default -l "component=worker,app=airflow" -o jsonpath="{.items[0].metadata.name}")

echo "Copying DAGs into $SCHEDULER_POD"
kubectl exec -it "$SCHEDULER_POD" -- rm -fr /opt/airflow/mounts/dags/*
kubectl cp ./dags/ "$SCHEDULER_POD":/opt/airflow/mounts/

echo "Copying DAGs into $WEB_POD"
kubectl exec -it "$WEB_POD" -- rm -fr /opt/airflow/mounts/dags/*
kubectl cp ./dags/ "$WEB_POD":/opt/airflow/mounts/

echo "Copying DAGs into $WORKER_POD"
kubectl exec -it "$WORKER_POD" -- rm -fr /opt/airflow/mounts/dags/*
kubectl cp ./dags/ "$WORKER_POD":/opt/airflow/mounts/
