/*
							SCHEMA TENTATIVO - Monitor Ciudad
										abril 10, 2020

Utilice el archivo EsquemaAlias.xls como guia para nombrar las variables. Dividi las variables en las siguientes secciones: 

	- Datos de Ubicacion
	- Agua
	- Servicio de Energía Eléctrica
	- Gas Domestico
	- Metadata

*/


------------------------------------------------------------------------------------------------------

/*
							Datos de Ubicacion


Preguntas: 
	- Usamos hidro, hidrologica, Seleccione su hidrologica regional o temp hidro? 
 

Variables en esta seccion: 

- entidad	
- hidro	
- hidro_descentralizada	
- temp_hidro	
- hidrologica	
- fecha_inicio	
- semana_reporte	
- dia_semana_inicio	
- inicio_semana	
- fin_semana



*/
------------------------------------------------------------------------------------------------------


SELECT 

DISTINCT JSON_EXTRACT(payload,   "$['GlobalID']")                      AS GlobalID, -- ID Unico para cada reporte. El Global ID lo hace automaticamente para cada reporte. El usuario no sabe de este ID.

JSON_EXTRACT(payload,   "$['\u00bfDesde qu\u00e9 regi\u00f3n nos reportas?' ]") AS entidad,
JSON_EXTRACT(payload,   "$['municipio']")                                       AS municipio,
JSON_EXTRACT(payload,   "$['Parroquia']")                                       AS parroquia,
JSON_EXTRACT(payload,   "$['Urbanizaci\u00f3n o Sector']")                      AS sector,
SAFE_CAST (JSON_EXTRACT(payload,   "$['coordx']") AS FLOAT64) 									AS coordx,
SAFE_CAST (JSON_EXTRACT(payload,   "$['coordy']") AS FLOAT64)  									AS coordy,
JSON_EXTRACT(payload,   "$['coord' ]") 											                    AS coord,

	-- hidro: Valor Calculado. Parece que esta es la hidrologica regional. Tiene varios en lugar de valores nulos (comparado con campo hidrologica)
-- JSON_EXTRACT(payload,   "$['hidro']")                                           AS hidro, -- 
JSON_EXTRACT(payload,   "$['hidrologica']") AS hidrologica, -- Tiene los mismos valores de hidro pero tiene valores nulos. 
-- JSON_EXTRACT(payload,   "$['fecha_inicio']")                                    AS fecha_inicio, -- Validar este campo
SAFE_CAST (JSON_EXTRACT(payload,   "$['Semana de monitoreo']") AS INT64) 						    AS semana_reporte,	 
SAFE_CAST (JSON_EXTRACT(payload,   "$['dia_semana_inicio']") AS FLOAT64) 								AS dia_semana_inicio, 

safe.PARSE_DATETIME("%m/%e/%Y %I:%M:%S %p" , 
REGEXP_REPLACE(JSON_EXTRACT(payload,   "$['D\u00eda Inicio']"), r'\"|\'', '') 
) AS inicio_semana,

safe.PARSE_DATETIME("%m/%e/%Y %I:%M:%S %p" , 
REGEXP_REPLACE(JSON_EXTRACT(payload,   "$['D\u00eda Fin']"), r'\"|\'', '') 
) AS fin_semana,

------------------------------------------------------------------------------------------------------

/*

						AGUA


Preguntas: 
- frecuencia_agua es medida en dias? 


Variables:

- suministro_semanal
- agua_interrupcion
- agua_dias
- horas_aguasemana:
	- horas_agua_lunes
	- horas_agua_martes 
	- horas_agua_miercoles
	- horas_agua_jueves
	- horas_agua_viernes
	- horas_agua_sabado
	- horas_agua_domingo

- horas_entresemana
- horas_finsemana
- ciclo1
- ciclo2
- horas_agua_semana
- dias_sin_agua
- frecuencia_agua
- numero_semanas
- acceso
- dias_tanque
- llenado_tanque
- horas_tanque
- costo_cisterna
- acarreo_cisterna
- acarreo
- costo_acarreo
- horas_acarreo
- higiene
- comentario_sinagua
- calidad_agua
- p_organoleptica
- potabilizacion
- potabilizacion_other
- pagar_aguabot




*/


SAFE_CAST(JSON_EXTRACT(payload,   "$['\u00bfRecibiste agua esta semana?']") AS INT64 )AS suministro_semanal,

SAFE_CAST(JSON_EXTRACT(payload,   "$['Indique cantidad de d\u00edas que pas\u00f3 sin agua antes de recibir nuevamente']") AS INT64) AS agua_interrupcion,

JSON_EXTRACT(payload,   "$['\u00bfQu\u00e9 d\u00eda de esta semana recibiste agua por las tuber\u00edas?']") AS agua_dias,

	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el lunes']") AS INT64)     AS horas_agua_lunes,
	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el martes']") AS INT64)   AS horas_agua_martes, 
	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el miercoles']") AS INT64) AS horas_agua_miercoles,
	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el jueves']") AS INT64)    AS horas_agua_jueves, 
	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el viernes']") AS INT64)   AS horas_agua_viernes,
	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el sabado']") AS INT64)    AS horas_agua_sabado,
	SAFE_CAST(JSON_EXTRACT(payload,   "$['Horas de agua recibidas el domingo']") AS INT64)   AS  horas_agua_domingo,


SAFE_CAST( JSON_EXTRACT(payload,   "$['horas_entresemana']") AS INT64) AS horas_entresemana,
SAFE_CAST( JSON_EXTRACT(payload,   "$['horas_finsemana']") AS INT64)   AS horas_finsemana,

-- Ciclos en los que hidrocapital envia agua en CCS. 
	SAFE_CAST (     JSON_EXTRACT(payload,   "$['ciclo1']") AS INT64) AS ciclo1, -- Ciclo1: Domingo a Miercoles
	SAFE_CAST ( 	JSON_EXTRACT(payload,   "$['ciclo2']") AS INT64) AS ciclo2, -- Ciclo2: Miercoles a Sabado

SAFE_CAST ( JSON_EXTRACT(payload,   "$['horas_aguasemana']") AS INT64) 								  					AS horas_agua_semana,
SAFE_CAST ( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1ntos d\u00edas llevas sin agua?']") AS INT64)   					AS dias_sin_agua,
SAFE_CAST ( JSON_EXTRACT(payload,   "$['\u00bfCon qu\u00e9 frecuencia sueles recibir el agua normalmente?']") AS INT64) AS frecuencia_agua, 
SAFE_CAST ( JSON_EXTRACT(payload,   "$['Indique el n\u00famero de semanas']") AS INT64) 								AS numero_semanas,

JSON_EXTRACT(payload,   "$['\u00bfC\u00f3mo obtuvo el agua para su casa esta semana?']") AS acceso,


SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1ntos d\u00edas sin agua puedes aguantar con lo almacenado en los tanques/envases?']") AS INT64) AS dias_tanque,
SAFE_CAST( JSON_EXTRACT(payload,   "$['llenado_tanque']") AS INT64) AS llenado_tanque,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1ntas horas dedic\u00f3 en la semana para recolectar agua?']") AS INT64) AS horas_tanque,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1nto debi\u00f3 pagar por el cisterna?']") AS INT64) AS costo_cisterna,

SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfDebi\u00f3 acarrear el agua en tobos?']") AS INT64) AS acarreo_cisterna,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfPag\u00f3 a alguien para que le llevara el agua a su hogar?']") AS INT64) AS acarreo,
SAFE_CAST( JSON_EXTRACT(payload,   "$['Monto total pagado']") AS INT64) AS costo_acarreo, 
SAFE_CAST( JSON_EXTRACT(payload,   "$['Tiempo dedicado a cargar agua']") AS INT64) AS horas_acarreo,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfConsidera usted que tuvo suficiente agua para garantizar las condiciones de higiene recomendadas para evitar la infecci\u00f3n del Covid-19?']") AS INT64) AS higiene,


JSON_EXTRACT(payload,   "$['Comentario']") AS comentario_sinagua,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfLa calidad del agua que recibi\u00f3 fue?']") AS INT64) AS calidad_agua,
SAFE_CAST( JSON_EXTRACT(payload,   "$['Indique las caracter\u00edsticas del agua que recibi\u00f3.']") AS INT64) AS p_organoleptica,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfC\u00f3mo garantiza usted que el agua que consume sea potable?']") AS INT64) AS potabilizacion,
JSON_EXTRACT(payload,   "$['Specify other.']") AS potabilizacion_other,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1nto gasto esta semana comprando agua potable?']") AS INT64) AS pagar_aguabot,


-----------------------------------------------------------------------------------------------------




/*
						Servicio de Energía Eléctrica

Variables
- interrupcion_elect
- velectricidad_dias
	- lunes_e
	- martes_e
	- miercoles_e
	- jueves_e
	- viernes_e
	- sabado_e
	- domingo_e
- apagon_dia_laboral
- apagon_finsemana
- apagon_semana
- bajones_e
- comentario_elec

*/


SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfTuvo interrupciones en el servicio el\u00e9ctrico esta semana?']") AS INT64) AS interrupcion_elect,

SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfQu\u00e9 d\u00edas de la semana tuvo interrupci\u00f3n del servicio el\u00e9ctrico?']") AS INT64) AS electricidad_dias,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido con servicio interrumpido el lunes']") AS INT64) AS lunes_e,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido el martes']") AS INT64) 		  AS martes_e,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido el mi\u00e9rcoles']") AS INT64) AS miercoles_e,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido el jueves']") AS INT64) 		  AS jueves_e,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido el viernes']") AS INT64)        AS viernes_e,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido el s\u00e1bado']") AS INT64)    AS sabado_e,
	SAFE_CAST( JSON_EXTRACT(payload,   "$['Horas con servicio interrumpido el domingo']") AS INT64)        AS domingo_e,

SAFE_CAST( JSON_EXTRACT(payload,   "$['apagon_dialaboral']") AS INT64) AS apagon_dia_laboral,
SAFE_CAST( JSON_EXTRACT(payload,   "$['apagon_finsemana']") AS INT64)  AS apagon_finsemana,
SAFE_CAST( JSON_EXTRACT(payload,   "$['apagon_semana']") AS INT64)     AS apagon_semana,

SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfEsta semana tuvo cambios de voltaje que pudieran ocasionar da\u00f1os en sus aparatos o equipos?']") AS INT64) AS bajones_e,
JSON_EXTRACT(payload,   "$['Comentario__1']") AS comentario_elec, 



------------------------------------------------------------------------------------------------------
/*

							Apartado 4: Gas Doméstico


Variables: 
- gas_semana
- gas_tiempo
- gas_tipobombona
- gas_costo
- gas_camion
- comentario_gas

*/
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfTuvo Gas Dom\u00e9stico esta semana?']") AS INT64) 									  AS gas_semana,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1nto tiempo estima que tardar\u00e1 en poder conseguir una bombona?']") AS INT64) AS gas_tiempo,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfQu\u00e9 tipo de bombona usa usted?']") AS INT64)										  AS gas_tipobombona,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfCu\u00e1nto estima le costar\u00e1 la bombona de gas?']") AS INT64)					  AS gas_costo,
SAFE_CAST( JSON_EXTRACT(payload,   "$['\u00bfLos camiones despachan gas en la comunidad?']") AS INT64)          					  AS gas_camion,
JSON_EXTRACT(payload,   "$['Comentario__2']") AS comentario_gas, 







------------------------------------------------------------------------------------------------------

/*

Metadata 

Considere metadata todo aquello relacionado a: 
	- Info personal (correo, ID, habitantes de parroquia, codigo monitor) como 
	- IDs o timestamps generados por el sistema (inicio_reporte, fin_reporte, tiempo_llenado,EditDate, GlobalID, CreationDate)
*/

JSON_EXTRACT(payload,   "$['Correo']")                 						AS mail,
JSON_EXTRACT(payload,   "$['C\u00f3digo de monitor']") 						AS cod_monitor, -- Esta todo null, por que?
SAFE_CAST( JSON_EXTRACT(payload,   "$['hab']") AS INT64)                    AS hab, -- Cuantas personas estan siendo afectadas en la parroquia. 


safe.PARSE_DATETIME("%m/%e/%Y %I:%M:%S %p" , 
REGEXP_REPLACE(JSON_EXTRACT(payload,   "$['inicio_reporte']"), r'\"|\'', '') 
) AS inicio_reporte, 

safe.PARSE_DATETIME("%m/%e/%Y %I:%M:%S %p" , 
REGEXP_REPLACE(JSON_EXTRACT(payload,   "$['fin_reporte']"), r'\"|\'', '') 
) AS fin_reporte,

SAFE_CAST(JSON_EXTRACT(payload,   "$['tiempo_llenado']") AS FLOAT64) AS tiempo_llenado, -- Tiempo que usuario lleno encuesta

SAFE_CAST(JSON_EXTRACT(payload,   "$['Seguimiento y Evaluaci\u00f3n']") AS INT64) AS seguimiento,
JSON_EXTRACT(payload,   "$['short_uid']")                     AS short_uid, -- UNIQUE ID Aleatorio que pides que genere. Solo te lo va a pedir si el usuario le da a esta. 

safe.PARSE_DATETIME("%m/%e/%Y %I:%M:%S %p" , 
REGEXP_REPLACE(JSON_EXTRACT(payload,   "$['CreationDate']"), r'\"|\'', '') 
) AS CreationDate,

safe.PARSE_DATETIME("%m/%e/%Y %I:%M:%S %p" , 
REGEXP_REPLACE(JSON_EXTRACT(payload,   "$['EditDate']"), r'\"|\'', '') 
) AS edit_date



FROM
  `event-pipeline.angostura_dev.raw_events`
WHERE 
  event = 'agua_2'
