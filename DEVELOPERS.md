# Developing for airflow-jobs

- [Initial Setup](#initial-setup)
- [Local Development](#local-development)

## Initial Setup

### Clone Gitlab Repositories

First, set up an account on Gitlab. We then recommend setting up an
[SSH Key](https://docs.gitlab.com/ee/ssh/) for your Git interactions. After
that you'll need to clone this repo and also is infra dependency:

```bash
git clone git@gitlab.com:codeforvenezuela/angostura/infra.git
git clone git@gitlab.com:codeforvenezuela/angostura/airflow-jobs.git
```

### Install Dependencies

You need to install the following dependencies to run this project locally:

- [Docker Compose](https://docs.docker.co/compose/install/).
- [Helm](https://helm.sh/docs/intro/install/).
- [Kubernetes](https://docs.docker.com/docker-for-mac/#kubernetes).
- [gcloud](https://cloud.google.com/sdk/install).

Make sure to use helm v2, as the commands in this documentation are specific to this version.

If this is your first working with helm, then you'll need to run the following, to add a repo to source the helms we need:

```bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm init
```

Also, make sure you are authenticated to GCP docker registry:

```
gcloud auth configure-docker
```

Last but not least, helm chart configs are in the INFRA repo. So before starting airflow locally, pull the latest version of the infra repo:


## Local Development

### 1. Set up GCP secret

Provided you have the required permissions, you can run the following to create
a Kubernetes secret object that you'll use to set up an Airflow GCP connection:

```bash
./scripts/create_gcp_secret.sh
```

### 2. Set up a ConfigMap for requirements.txt

You need to set up a [ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/) for `requirements.txt`
as later it will be required to install extra dependencies for DAGs.

```bash
kubectl create configmap py-requirements --from-file=requirements.txt
```

If you need to recreate the `configmap`, you can issue the following commands:

```bash
kubectl config use-context docker-desktop
kubectl delete configmap py-requirements
kubectl create configmap py-requirements --from-file=requirements.txt
```

### 3. Install Airflow Helm Chart

We maintain the base Helm config
[here](https://gitlab.com/codeforvenezuela/angostura/infra/blob/master/charts/airflow/values.yml),
but by itself is not enough to allow testing local DAGs, and that's why the
override file [values.local.yml](./values.local.yml) is provided. Assuming you
cloned both this and the infra repo locally, you could run something like the
following to spin up a local Airflow Kubernetes cluster:

```bash
# Make sure infra repo is also up to date
helm install airflow stable/airflow -f ../infra/charts/airflow/values.yml -f values.local.yml --version 7.2.0
```
In helm v2.14 the chart name is specified via --name. In this case, you need to do:

```bash
helm install --name airflow stable/airflow -f ../infra/charts/airflow/values.yml -f values.local.yml --version 7.2.0
```

Make sure all Pods get into `READY` state with `kubectl get pods`. It
can take a couple of minutes, the web server is expected to be a bit slow
to initialize.

### 4. Uploading DAGs

The cluster is initialized without DAGs, but you can upload them with the
following (you'll need to re-run for any change):

```bash
./scripts/upload_dags.sh
```

Sometimes this will fail because the airflow scheduler will be restarting
when you execute it. Until we find a better method to bake local DAGs folder
to the pods, then for now you'll just have to try again after it comes back.

### 5. Connect to the Airflow Web Console to execute DAGs

This script will set up a forward connection into the Airflow web console
through http://localhost:8080:

```bash
./scripts/forward_web.sh
```

You'll want to visit the console to check logs and enable the DAGs you want
executed as by default they're disabled.

#### Configure GCP connection

At this point you should have a `/secrets/` volume configured. Use the Airflow
web console to create a connection named `gcp_production` that uses the `/secrets/.cronjob-sa.json`
credentials to connect into production. **This means you're now in position to
affect production, so be careful!!!**. Don't get used to it thought, we're
probably gonna work on isolation soon.

Below is how this connection should look:

![GCP connection](./images/gcp_conn_example.png)
