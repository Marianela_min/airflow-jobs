# Airflow Jobs

Repository containing Airflow DAGs definition for Angostura jobs

## Develop
We've set up a separate document for [developers](DEVELOPERS.md).

## Guides

* If you want to add another table, read [Add a new BigQuery Table](https://www.notion.so/Add-a-new-BigQuery-table-f3c1f072476b4f4187ed376fcf30fc43)

## License

This project is licensed under the MIT License - see the
[LICENSE.md](LICENSE.md) file for details
